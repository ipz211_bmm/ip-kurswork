<?php

namespace forms;

use core\FormValidator;

class LoginForm extends FormValidator
{

    public string $login='';
    public string $password='';

    public function setRules(): array
    {
        return [
          'login'=>[self::REQUIRED,self::HIDDEN],
          'password'=>[self::REQUIRED,self::HIDDEN]
        ];
    }
}