<?php

namespace forms;

use core\FormValidator;

class ReportForm extends FormValidator
{
    public string $description='';
    public string $category='';

    public function setRules(): array
    {
        return [
            'category'=>[self::REQUIRED],
            'description'=>[self::REQUIRED,self::MIN_LEN=>10]
        ];
    }
}