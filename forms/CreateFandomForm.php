<?php

namespace forms;

use core\FormValidator;
use JetBrains\PhpStorm\ArrayShape;

class CreateFandomForm extends FormValidator
{
    public string $category='';
    public string $title='';
    public string $content='';

    #[ArrayShape(['title' => "array"])]
    public function setRules(): array
    {
        return [
            'title'=>[self::REQUIRED,self::MIN_LEN=>4]
        ];
    }
}