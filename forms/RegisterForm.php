<?php

namespace forms;

class RegisterForm extends \core\FormValidator
{

    public string $firstname='';
    public string $login='';
    public string $surname='';
    public string $email='';
    public string $password='';

    public function setRules(): array
    {

        return [
            'email'=>[self::EMAIL],
            'password'=>[self::REQUIRED,self::MIN_LEN=>5,self::HIDDEN],
            'login'=>[self::REQUIRED,self::MIN_LEN=>4]
        ];
    }
}