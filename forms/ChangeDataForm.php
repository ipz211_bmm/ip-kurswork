<?php

namespace forms;

use core\FormValidator;

class ChangeDataForm extends FormValidator
{
    public string $firstname;
    public string $surname;
    public string $login='';
    public string $email='';
    public function setRules(): array
    {
        return [
            'email'=>[self::EMAIL,self::REQUIRED],
            'login'=>[self::REQUIRED,self::MIN_LEN=>4],
        ];
    }
}