<?php

namespace controllers;

use core\Controller;
use core\Core;
use JetBrains\PhpStorm\ArrayShape;
use models\fandom;
use repositories\CategoryRepository;
use repositories\FandomRepository;

use repositories\PhotoRepository;
use repositories\SubtopicRepository;
use repositories\TopicRepository;
use repositories\UserFandomRepository;
use repositories\FandomSubscribeRepository;

class FandomController extends Controller
{
    protected FandomRepository $repo;
    protected UserFandomRepository $fanRepo;
    protected CategoryRepository $categoryRepo;
    protected TopicRepository $topicRepository;
    protected FandomSubscribeRepository $fandomSubscribeRepository;
    protected SubtopicRepository $subRepo;
    public function __construct()
    {
        parent::__construct();
        $this->repo = new FandomRepository();
        $this->fanRepo = new UserFandomRepository();
        $this->categoryRepo = new CategoryRepository();
        $this->topicRepository = new TopicRepository();
        $this->fandomSubscribeRepository = new FandomSubscribeRepository();
        $this->subRepo = new SubtopicRepository();
    }

    public function editorAction($id): string
    {
        $topicAndSubtopics=[];
        $topicTitleId  = [];
        if (!$_SESSION['user']) {
            $this->redirect('/');
        }
        $topics = $this->topicRepository->getFandomTopicsById($id);
        foreach ($topics as $key=>$topic){
            $topicAndSubtopics[$topic['title']]=$this->topicRepository->getSubtopicsByTopicId($topic['id']);
            $topicTitleId[$topic['title']]=$topic['id'];
        }
        return $this->render(args: ['fandom' => $this->repo->getFandomById($id),
            'topics'=>$topicAndSubtopics,
            'ids'=>$topicTitleId]);
    }

    public function createAction(): string
    {
        if (Core::getInstance()->requestMethod === "POST") {
            $file = null;
            if (!empty($_FILES)) {
                $file = $_FILES['img']['tmp_name'];
            }
            if ($this->repo->addFandom($_POST, $file)) {
                $fan = $this->repo->getFandomByName($_POST['title']);
                $this->fanRepo->addUserFandom($_SESSION['user']['id'], $fan['id']);
                $this->redirect('/user/studio');
            }
        }
        return $this->render(null, ['form' => $this->repo->createForm, 'categories' => $this->categoryRepo->getCategories()]);
    }
    public function pageAction(int $id):string{
        if(!$this->repo->getFandomById($id)){
            $this->redirect('/');
        }
        $isUserFandom = false;
        $isUserSubscribed = false;
        if(isset($_SESSION['user'])){
            $isUserFandom = $this->fanRepo->isUserFandom($id,$_SESSION['user']['id']);
            $isUserSubscribed = $this->fandomSubscribeRepository->isUserSubscribed($_SESSION['user']['id'],$id);
        }
        $topicAndSubtopics=[];
        $topics = $this->topicRepository->getFandomTopicsById($id);
        foreach ($topics as $key=>$topic){
            $topicAndSubtopics[$topic['title']]=$this->topicRepository->getSubtopicsByTopicId($topic['id']);
        }

        return $this->render(args: ['fandom' => $this->repo->getFandomById($id),
            'isUserSubscribed'=>$isUserSubscribed,'isUserFandom'=>$isUserFandom,
            'topics'=>$topicAndSubtopics]);
    }
    #[ArrayShape(['action' => "bool"])]
    public function updateAction(): array
    {
        if(Core::getInstance()->requestMethod==="GET"){
            $this->redirect('/');
        }
        return ['action' => $this->repo->updateFandomBody($_POST)];
    }
    public function deleteAction():array{
        if(Core::getInstance()->requestMethod==="GET"){
            $this->redirect('/');
        }
        return ['action'=>$this->repo->removeFandom($_POST['id'])];
    }
    public function updatePhotoAction():array{
        if(Core::getInstance()->requestMethod==="GET"){
            $this->redirect('/');
        }
        return ['action'=>$this->repo->updatePhoto($_POST['id'],$_FILES['img']['tmp_name'])];
    }
    public function hiddenAction():array{
        $fandom = $this->repo->getFandomById($_POST['id']);
        if($fandom['hidden']==$_POST['hide']){
            return ['action'=>false,'msg'=>'already set'];
        }
        return ['action'=>$this->repo->toggleHidden($_POST['id'],$_POST['hide'])];
    }


}