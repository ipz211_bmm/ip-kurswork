<?php

namespace controllers;

use core\Controller;
use repositories\FandomRepository;
use repositories\SubtopicRepository;
use repositories\TopicItemsRepository;
use repositories\TopicRepository;

class SubtopicController extends Controller
{
    protected SubtopicRepository $repository;
    protected TopicItemsRepository $itemsRepository;
    protected TopicRepository $topicRepo;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new SubtopicRepository();
        $this->itemsRepository = new TopicItemsRepository();
        $this->topicRepo = new TopicRepository();
    }

    public function editAction(array $params): string
    {
        if(!isset($_SESSION['user'])){
            $this->redirect('/');
        }
        if (!$this->topicRepo->getTopicById($params['topic'])) {
            $this->redirect('/');
        }
        if (isset($params['subtopic'])) {
            $subtopic = $this->repository->getSubtopicById($params['subtopic']);
            if ($subtopic) {
                return $this->render(args: ['subtopic' => $subtopic]);
            }
        } else {
            return $this->render(args: ['subtopic' => null]);
        }
        $this->redirect('/');
    }

    public function addOrUpdateAction(string $id): array
    {
        if ($this->repository->getSubtopicById($id)) {
            $this->repository->updateSubtopic($id, $_POST['content']);
            return ['update' => 'successfully','action'=>true];
        }
        $this->repository->addSubtopic($_POST['title'], $_POST['ulr'], $_POST['content']);
        $subtopicId = $this->repository->getLastSubtopicByName($_POST['title']);
        $this->itemsRepository->addTopicItem($id, $subtopicId);
        return ['create' => 'successfully','action'=>true];
    }
    public function allAction($id):string{
        return $this->render(args:['subtopics'=>$this->topicRepo->getSubtopicsByTopicId($id)]);
    }
    public function deleteAction($id):array{
        return ['action'=>$this->repository->deleteSubtopic($id)];
    }
    public function pageAction($id)
    {
        if(!$this->repository->getSubtopicById($id)){
            $this->redirect('/');
        }
        return $this->render(args: ['subtopic' => $this->repository->getSubtopicById($id)]);
    }

}