<?php

namespace controllers;

use core\Controller;
use core\Core;
use repositories\FandomRepository;

class MainController extends Controller
{
    private FandomRepository $repo;
    public function __construct()
    {
        parent::__construct();
        $this->repo= new FandomRepository();
    }

    public function mainAction():string{
        return $this->render('main',
           ['fandoms'=>$this->repo->getFandoms()]);
    }
    public function fetchAction($limit):string{
        return $this->render('main',
            ['fandoms'=>$this->repo->getFandoms($limit)]);
    }

    public function errorAction():void{
        echo "404 not found!";
    }
}