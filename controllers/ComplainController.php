<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\complain_category;
use repositories\ComplainCategoryRepository;
use repositories\FandomComplain;

class ComplainController extends Controller
{
    protected ComplainCategoryRepository $category;
    protected  FandomComplain $complain;
    public function __construct()
    {
        parent::__construct();
        $this->category = new ComplainCategoryRepository();
        $this->complain = new FandomComplain();
    }
    public function createAction(string $id):string{
        if(Core::getInstance()->requestMethod==='POST'){
            $category = $this->category->getCategoryByName($_POST['category']);
            $this->complain->addComplain($category['id'],$id,$_POST['description']);
            $this->redirect("/fandom/page/$id");
        }
        $categories = $this->category->getCategories();
        $userComplains = $this->complain->getUserComplains($_SESSION['user']['id'],$id);
        return $this->render(args:['categories'=>$categories,'form'=>$this->complain->reportForm,
            'user_complains'=>$userComplains]);
    }
    public function listAllAction():string{
        $complains = $this->complain->getComplains();
       return $this->render(args:['complains'=>$complains]);
    }
    public function fandom_listAction($id):string{
        $complains = $this->complain->getFandomComplain($id);
        return $this->render(args:['complains'=>$complains]);
    }



}