<?php

namespace controllers;

use core\Controller;
use JetBrains\PhpStorm\ArrayShape;
use repositories\PhotoRepository;

class PhotoController extends Controller
{
    protected PhotoRepository $repo;

    public function __construct()
    {
        parent::__construct();
        $this->repo = new PhotoRepository();
    }

    #[ArrayShape(['photosInfo' => "array"])]
    public function addAction(): array
    {
        $response = $this->repo->addPhotos($_POST['id'], $_FILES);
        return ['photosInfo' => $response];
    }
    public function photogaleryAction(int $id):string{
        return $this->render('views/fandom/photogalery',['photos'=>$this->repo->getPhotosByFandomId($id)]);
    }
}