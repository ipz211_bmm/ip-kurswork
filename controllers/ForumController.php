<?php

namespace controllers;

use core\Controller;
use core\Core;
use repositories\AnswerRepository;
use repositories\PostLikeRepository;
use repositories\PostRepository;
use repositories\UserCommentsLikesRepo;
use repositories\UserPostLikesRepository;

class ForumController extends Controller
{
    protected PostRepository $postRepository;
    protected AnswerRepository $answerRepository;
    protected PostLikeRepository $likeRepository;
    protected UserPostLikesRepository $postlike;
    protected UserCommentsLikesRepo $comlike;
    public function __construct()
    {
        parent::__construct();
        $this->postRepository = new PostRepository();
        $this->answerRepository = new AnswerRepository();
        $this->likeRepository = new PostLikeRepository();
        $this->postlike  =  new UserPostLikesRepository();
        $this->comlike = new UserCommentsLikesRepo();

    }

    public function postAction($id):string{
        if(Core::getInstance()->requestMethod==='POST'){
            $this->postRepository->addPost($id,$_SESSION['user']['id'],$_POST['content']);
        }
        $posts  = $this->postRepository->getFandomPosts($id);
        foreach ($posts as $key=>$value){
            $answers = $this->postRepository->getAnswersByPostId($value['id']);
            $posts[$key]['answers']=$answers;
        }
        return $this->render(args:['posts'=>$posts]);
    }
    public function answerAction($id):array{
      return ['action'=>$this->answerRepository->addAnswer($id,$_SESSION['user']['id'],$_POST['answer'],$_POST['receiver_id']??null)];
    }
    public function LikeAction():array{
        if(!$this->postlike->getPostLikes($_SESSION['user']['id'],$_POST['id'])) {
            return ['action' => $this->likeRepository->addLike($_SESSION['user']['id'], $_POST['id']),'msg'=>'like'];
        }
        return ['action'=>$this->likeRepository->removeLike($_POST['id']),'msg'=>'deleted'];
    }
    public function LikeAnswerAction():array{
        if(!$this->comlike->getPostLikes($_SESSION['user']['id'],$_POST['id'])) {
            return ['action_a' => $this->answerRepository->addLike($_SESSION['user']['id'], $_POST['id']), 'msg' => 'like_a'];
        }
        return ['action_a'=>$this->answerRepository->removeLike($_POST['id']),'msg'=>'deleted_a'];
    }

}