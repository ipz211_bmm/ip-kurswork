<?php

namespace controllers;


use core\Controller;
use core\Core;
use repositories\FandomSubscribeRepository;

class SubscribeController extends Controller
{
    protected FandomSubscribeRepository $repository;
    protected array $response=[];
    public function __construct(){
        parent::__construct();
        if(Core::getInstance()->requestMethod==='GET'){
           $this->redirect('/');
        }
        $this->repository = new FandomSubscribeRepository();
    }
    public function addAction():array{
        $this->response[] = $this->repository->addSubscriber($_POST['fandom_id'],$_SESSION['user']['id']);
        return $this->response;
    }
    public function removeAction():array{
        $this->response[] = $this->repository->removeSubscriber($_POST['fandom_id'],$_SESSION['user']['id']);
        return $this->response;
    }
}