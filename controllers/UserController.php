<?php

namespace controllers;

use core\Controller;
use core\Core;
use JetBrains\PhpStorm\NoReturn;
use models\user;
use repositories\PictureRepository;
use repositories\UserRepository;



class UserController extends Controller
{
    private UserRepository $repo;
    private array|bool $msg;
    public function __construct()
    {
        parent::__construct();
        $this->repo = new UserRepository();
    }

    public function registerAction(): string
    {
        if($this->repo->isUserAuthenticated()){
            $this->redirect('/');
        }
        if (Core::getInstance()->requestMethod === 'POST') {
            if (!$this->repo->getUserBy('login',$_POST['login'])&&!$this->repo->getUserBy('email',$_POST['email'])) {
                if($this->repo->addUser($_POST)) {
                    $this->redirect('/');
                }
            }else{
                $this->repo->registerForm->errors['msg']='Invalid email or login';
            }
        }
        return $this->render('register', ['form'=>$this->repo->registerForm]);
    }
    public function loginAction():string{
        if($this->repo->isUserAuthenticated()){
            $this->redirect('/');
        }
        if(Core::getInstance()->requestMethod==='POST') {
            if ($this->repo->login($_POST)) {
                $this->redirect('/');
            }
        }
        return $this->render('login',['form'=>$this->repo->loginForm]);
    }

    #[NoReturn]
    public function logoutAction():void{
        $this->repo->logout();
        $this->redirect('/');
    }
    public function mainAction(): string
    {
        return $this->render();
    }
    public function settingAction():string{
        if(Core::getInstance()->requestMethod==='POST') {
            $file = null;
            if (!empty($_FILES)) {
                $file = $_FILES['img']['tmp_name'];
            }
            $this->repo->updateUser($_POST, $file);
            $this->redirect('/user/setting');
        }
        $this->repo->dataForm->loadData($_SESSION['user']);
        return $this->render(args: ['user'=>$_SESSION['user'],'form'=>$this->repo->dataForm]);
    }

    public function studioAction():string{
        return $this->render(null,['fandoms'=>$this->repo->testJoin()]);
    }

}