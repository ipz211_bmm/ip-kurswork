<?php

namespace controllers;

use core\Controller;
use repositories\VideoRepository;

class VideoController extends Controller
{
    protected VideoRepository $repo;
    public function __construct()
    {
        parent::__construct();
        $this->repo = new VideoRepository();
    }

    public function addAction():array{
        return ['photosInfo'=>$this->repo->addVideos($_POST['id'],$_FILES)];
    }
}