<?php

namespace controllers;

use core\Controller;

use repositories\TopicRepository;

class TopicController extends Controller
{
    protected TopicRepository $repo;
    public function __construct()
    {
        parent::__construct();
        $this->repo = new TopicRepository();
    }
    public function addAction():array{
        return ['action'=> $this->repo->addTopic($_POST['id'],$_POST['title'])];
    }
    public function getAction(int $id):array{
        return ['topics'=>$this->repo->getFandomTopicsById($id)];
    }
    public function deleteAction($id):array{
        return ['action'=>$this->repo->removeTopic($id)];
    }
}