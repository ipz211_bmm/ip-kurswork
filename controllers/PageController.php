<?php

namespace controllers;

use core\Controller;
use models\topic;
use repositories\TopicRepository;

class PageController extends Controller
{
    protected TopicRepository $repo;
    public function __construct()
    {
        parent::__construct();
        $this->repo =new TopicRepository();
    }

    public function addAction(int $id):string{
        return $this->render(args: ['topic'=>$this->repo->getTopicById($id)]);
    }
}