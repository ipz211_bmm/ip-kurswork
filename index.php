<?php
use core\Db;
use models\User;

spl_autoload_register(function ($className){
     $path = $className.'.php';
     if (is_file($path)) {
        require($path);
     }
});

$core = core\Core::getInstance();
$core->Initialize();
$core->Run();
echo $core->Done();