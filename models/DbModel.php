<?php

namespace models;
use core\Db;
use core\FormValidator;

class DbModel
{
    private \PDO $pdo;
    private string $className;
    private string $stmt;
    public array $mapFields;
    private string $where;
    private array $condArray=[];
    public function __construct(){
        $this->pdo=Db::getDb();
        $this->className=(new \ReflectionClass($this))->getShortName();
    }
    public function select(string|array $fields="*"):DbModel{
        if (is_array($fields)){
            $fields=implode(separator: ', ', array: $fields);
        }
        $this->stmt=/** @lang text */ "SELECT $fields from $this->className";
        return $this;
    }
    public function get(bool $all=true):array|bool{
        $res = $this->pdo->prepare($this->stmt);
        /*echo "<pre>";
        echo $this->stmt;
        echo "</pre>";*/
        $ex = $res->execute($this->condArray);
        if (!$ex){
            return false;
        }
        $this->condArray=[];
        $this->stmt = '';
        if($all) {
            return $res->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return $res->fetch(\PDO::FETCH_ASSOC);
        }
    }

    public function where($condition,string $operator='='):DbModel{
        if(is_array($condition)){
            $this->condArray=$condition;
            $parts =[];
            foreach ($condition as $key=>$value){
                $parts[]="$this->className.$key $operator :$key";
            }
            if(!empty($this->stmt))
                $this->stmt.= " WHERE ".implode(" AND ",$parts);
            else
                $this->where = " WHERE ".implode(" AND ",$parts);
        }
        return $this;
    }
    public function update(array $newValues):array|bool{
        if(empty($this->condArray)){
            return false;
        }
        $setStmt=[];
        foreach ($newValues as $key=>$value){
            $setStmt[] = "$key = :$key";
        }
        $setStmt = implode(', ', $setStmt);
        $this->condArray = array_merge($this->condArray,$newValues);
        $this->stmt = /** @lang text */ "UPDATE $this->className SET $setStmt".$this->where;
        $res = $this->pdo->prepare($this->stmt);

        return $res->execute($this->condArray);
    }
    public function insert(array $fields):bool{
        $keys = array_keys($fields);
        $keysStr = implode(', ',$keys);
        foreach ($fields as $key=>$values){
          $this->condArray[] = ':'.$key;
        }
        $values = implode(', ',$this->condArray);
        $res = $this->pdo->prepare(/** @lang text */ "INSERT INTO $this->className ($keysStr) VALUES($values)");
        $this->condArray=[];
        return $res->execute($fields);
    }
    public function delete():bool{
        if(!empty($this->condArray)) {
            $res = $this->pdo->prepare(/** @lang text */ "DELETE from $this->className" . $this->where);
            return $res->execute($this->condArray);
        }
        return false;
    }
    public function join(string|DbModel $tableName,array|string $condition):DbModel{
        $condPart = [];
        foreach ($condition as $key=>$value){
            $condPart[]="$key = $value";
        }
        $condPart = implode(', ',$condPart);
        $this->stmt.=" INNER JOIN $tableName ON $condPart";

        return $this;
    }
    public function orderBy(string $field,string $rule="ASC"):DbModel{
        $this->stmt.=" ORDER BY $field $rule";

        return $this;
    }
    public function limit($limit):DbModel{
        $this->stmt.=" LIMIT $limit";
        return $this;
    }

}