<?php

namespace models;

use models\DbModel;
use core\FormValidator;

class user extends DbModel
{
    public string $id;
    public array $mapFields=['user.id','firstname','surname','login','email','img'];
}