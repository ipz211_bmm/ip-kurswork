<?php
/* @var $title * */
/* @var $content * */
/* @var $user * */
?>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 charset="">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/src/styles/base.css">
    <link rel="stylesheet" href="/src/styles/header.css">
    <?php if(isset($user)): ?>
    <script src="/src/scripts/account.js" defer></script>
    <?php endif; ?>
    <script src="/src/scripts/main.js" defer></script>
</head>
<body>
<header>
    <div>
        <a href="/">Main</a>
        <a href="#">News</a>
        <a href="#">Відео</a>
        <a href="#">Something</a>
    </div>
    <div>
        <?php if (!isset($user)): ?>
            <a href="/user/login">Login</a>
            <a href="/user/register">Register</a>
        <?php else: ?>
            <img src="/static/site/notification-bell.png" alt="bell" class="animate-img">
            <img src="<?='/files/user/'.$user['img']?>"  alt="not set" class="animate-img">
        <?php endif; ?>
    </div>
</header>
<h3 class="msg"></h3>
<div class="account notifications">
    <div class="section">
        <span>New subscriber</span>
    </div>
    <div class="section">
        <a href='#'>
            <img src="/static/site/next.png" alt="setting">
            New subscriber
        </a>
    </div>
    <div class="section">
        <span>New subscriber</span>
    </div>
</div>
<div class="account">
    <div class="section">
        <img src="<?='/files/user/'.$user['img']?>"  alt="no set" class="account-img">
        <span><?=$user['login']??''?></span>
    </div>
    <div class="section">
        <a href='/user/setting'>
            <img src="/static/site/setting-black.png" alt="setting">
            Settings
        </a>
        <a href='/user/studio'>
            <img src="/static/site/chroma.png" alt="setting">
            Studio
        </a>
    </div>
    <?php if(isset($_SESSION['user']['permissions'])&&in_array(1,$_SESSION['user']['permissions'])): ?>
    <div class="section">
        <a href="/complain/listall">
            <img src="/static/site/complaint.png" alt="setting">
            Fandom Reports
        </a>
    </div>
    <?php endif; ?>
    <div class="section">
        <a href="/user/logout">
            <img src="/static/site/exit.png" alt="setting">
            Exit
        </a>
    </div>
</div>
<?= $content ?>
</body>
</html>