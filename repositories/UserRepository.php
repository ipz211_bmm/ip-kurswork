<?php

namespace repositories;


use Cassandra\Map;
use core\FileWorker;
use core\Mapper;
use forms\ChangeDataForm;
use forms\LoginForm;
use models\user;
use forms\RegisterForm;

class UserRepository
{
    public RegisterForm $registerForm;
    public LoginForm $loginForm;
    public ChangeDataForm $dataForm;
    private user $user;
    private FileWorker $fileWorker;
    public function __construct()
    {
        $this->registerForm = new RegisterForm();
        $this->loginForm = new LoginForm();
        $this->dataForm = new ChangeDataForm();
        $this->user = new user();
        $this->fileWorker = new FileWorker('files/user','.jpg');
    }

    public function addUser($data): bool
    {
        $this->registerForm->loadData($data);
        if ($this->registerForm->validate()) {
            $this->user->insert(fields: ['firstname' => $_POST['firstname'], 'surname' => $_POST['surname'],
                'email' => $_POST['email'], 'login' => $_POST['login'], 'password' => crypt($_POST['password'], "$5\$${_POST['login']}$")]);
            return true;
        }
        return false;
    }

    public function login($data): bool
    {
        $this->loginForm->loadData($data);
        if ($this->loginForm->validate()) {
            $res = $this->getUserBy('email', $_POST['login']);
            if(!$res) {
                $res = $this->getUserBy('login', $_POST['login']);
            }
            if ($res) {
                if (password_verify($_POST['password'], $res['password'])) {
                    $this->authenticate($res);
                    return true;
                }
            }
            $this->loginForm->errors['msg'] = 'Invalid login or password';
        }
        return false;
    }

    public function authenticate($data): void
    {
        $this->user->id = $data['id'];
        $mapper = new Mapper($this->user);
        $_SESSION['user'] = $mapper->map();
        $permissions = $this->getPermissions($_SESSION['user']['id']);
        foreach ($permissions as $key=>$permission){
            $_SESSION['user']['permissions'][]=$permission['permission_id'];
        }
    }

    public function logout(): void
    {
        unset($_SESSION['user']);
    }

    public function isUserAuthenticated(): bool
    {
        return isset($_SESSION['user']);
    }

    public function updateUser($data,$img=null): bool
    {
        $this->duplicateFileDelete();
        if($img) {
            $data['img'] = $this->fileWorker->loadFile($img);
        }
        $this->dataForm->loadData($data);
        if($this->dataForm->validate()) {
            if($this->sessionUpdate($data)) {
                return $this->user->where(['id' => $_SESSION['user']['id']])->update($data);
            }
        }
        return false;
    }
    private function sessionUpdate($data):bool{
        $flag=false;
        foreach ($data as $key => $value) {
            if($_SESSION['user'][$key]!==$value) {
                $_SESSION['user'][$key] = $value;
                $flag=true;
            }
        }
        return $flag;
    }
    public function duplicateFileDelete():void{
        $userImg = $_SESSION['user']['img'];
        if(isset($_SESSION['user']['img'])){
            unlink("file/user/$userImg");
        }
    }
    public function getAuthenticatedUser(): user
    {
        return $_SESSION['user'];
    }

    public function getUserBy(string $field, string $data): array|bool
    {
        $res = $this->user->select()->where([$field => $data])->get(false);
        if ($res) {
            return $res;
        }
        return false;
    }
    public function testJoin():array|bool{
        return $this->user->select(['fandom.*'])
            ->join('user_fandom',['user.id'=>'user_fandom.user_id'])
            ->join('fandom',['fandom.id'=>'user_fandom.fandom_id'])
            ->where(['id'=>$_SESSION['user']['id']])->get();
    }

    public function getPermissions($id):array|bool
    {
        return $this->user->select('user_permission.permission_id')
            ->join('user_permission',['user.id'=>'user_permission.user_id'])
            ->where(['id'=>$id])->get();
    }

}