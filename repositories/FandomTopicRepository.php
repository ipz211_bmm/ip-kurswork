<?php

namespace repositories;

use models\fandom_topic;

class FandomTopicRepository
{
    private fandom_topic $topic;
    public function __construct()
    {
        $this->topic = new fandom_topic();
    }
    public function addFandomTopic(int $fandomId,int $topicId):bool{
        return $this->topic->insert(['fandom_id'=>$fandomId,'topic_id'=>$topicId]);
    }
}