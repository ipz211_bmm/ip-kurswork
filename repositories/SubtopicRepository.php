<?php

namespace repositories;

use models\subtopic;

class SubtopicRepository
{
    protected subtopic $subtopic;
    public function __construct()
    {
        $this->subtopic = new subtopic();
    }
    public function addSubtopic($title,$url,$content):bool{
        return $this->subtopic->insert(['title'=>$title,'ulr'=>$url,'content'=>$content]);
    }
    public function updateSubtopic(int $id, $content):bool{
        return $this->subtopic->where(['id'=>$id])->update(['content'=>$content]);
    }
    public function getSubtopicById($id):array|bool{
        return $this->subtopic->select()->where(['id'=>$id])->get(false);
    }
    public function isExistSubtopics($topicId):bool|array{
        return $this->subtopic->select('subtopic.id')
            ->join('topic_item',['topic_item.topic_id'=>$topicId])
            ->get();
    }
    public function deleteSubtopic($id):bool{
      return  $this->subtopic->where(['id'=>$id])->delete();
    }
    public function getLastSubtopicByName($name):int|bool{
        $res = $this->subtopic->select('id')->where(['title'=>$name])->orderBy('id','DESC')->limit(1)->get(false);
        if(is_array($res)){
            return $res['id'];
        }
        return false;
    }


}