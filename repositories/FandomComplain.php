<?php

namespace repositories;

use forms\ReportForm;
use models\fandom_complain;

class FandomComplain
{
    protected fandom_complain $fandom_complain;
    public ReportForm $reportForm;
    public function __construct()
    {
         $this->fandom_complain= new fandom_complain();
         $this->reportForm = new ReportForm();
    }
    public function addComplain(int $categoryId,int $fandomId,string $description):bool{
        return $this->fandom_complain->insert(['category_id'=>$categoryId,'fandom_id'=>$fandomId,
            'description'=>$description,
            'sender_id'=>$_SESSION['user']['id']]);
    }
    public function getFandomComplain($id):array{
        return $this->fandom_complain->select('fandom.*,complain_category.name,fandom_complain.description')
            ->join('fandom',['fandom.id'=>'fandom_complain.fandom_id'])
            ->join('complain_category',['complain_category.id'=>'fandom_complain.category_id'])
            ->where(['fandom_id'=>$id])
            ->get();
    }
    public function getComplains():array|bool{
        return $this->fandom_complain->select('fandom.*,complain_category.name,fandom_complain.description')
            ->join('fandom',['fandom.id'=>'fandom_complain.fandom_id'])
            ->join('complain_category',['complain_category.id'=>'fandom_complain.category_id'])
            ->get();
    }

    public function getUserComplains($user_id,$fandom_id):bool|array
    {
        return $this->fandom_complain->select('sender_id')
            ->where(['sender_id'=>$user_id,'fandom_id'=>$fandom_id])->get(false);
    }
}