<?php

namespace repositories;

use models\topic;
use models\topic_item;

class TopicRepository
{
    protected topic $topic;
    public function __construct(){
        $this->topic = new topic();
    }

    public function addTopic(int $id,string $title):bool{
       return $this->topic->insert(['fandom_id'=>$id,'title'=>$title]);
    }

    public function getFandomTopicsById($id):array{
        return $this->topic->select('*')->where(['fandom_id'=>$id])->get();
    }
    public function getTopicById(int $topicId):array|bool{
        return $this->topic->select('*')->where(['id'=>$topicId])->get(false);
    }
    public function removeTopic($id):bool{
        return $this->topic->where(['id'=>$id])->delete();
    }
    public function getSubtopicsByTopicId($topicId){
        return $this->topic->select(['subtopic.title,subtopic.ulr,subtopic.id,topic.id as top_id'])
            ->join('topic_item',['topic_item.topic_id '=>'topic.id'])
            ->join('subtopic',['topic_item.subtopic_id'=>'subtopic.id'])
            ->where(['id'=>$topicId])
            ->get();
    }
    public function getSubtopicsById(int $topicId){
        return $this->topic->select('subtopic.title')
            ->join('topic_item',['topic_item.topic_id'=>'topic.id'])
            ->join('subtopic',['subtopic.id'=>'topic_item.subtopic_id'])
            ->where(['id'=>$topicId])
            ->get();
    }
}