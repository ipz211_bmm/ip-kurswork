<?php

namespace repositories;

use models\fandom;
use models\fandom_subscribers;

class FandomSubscribeRepository
{
    protected fandom_subscribers $subscribers;
    protected fandom $fandom;

    public function __construct()
    {
        $this->subscribers = new fandom_subscribers();
        $this->fandom = new fandom();
    }

    public function addSubscriber(int $fandomId, int $userId): array
    {
        if ($this->getFandomSubscriberById($userId, $fandomId)) {
            return ['add' => false, 'msg' => 'user already subscribed'];
        }
        if ($this->subscribers->insert(['fandom_id' => $fandomId, 'user_id' => $userId])) {
            return ['add' => true, 'msg' => 'user added'];
        }
        return ['add' => false, 'msg' => 'something wrong'];
    }

    public function removeSubscriber(int $fandomId, int $userId): array
    {
        if ($this->getFandomSubscriberById($userId, $fandomId)) {
            if ($this->subscribers->where(['user_id' => $userId, 'fandom_id' => $fandomId])->delete()) {
                return ['remove' => true, 'msg' => 'user subscribe removed'];
            }
        }
        return ['remove' => false, 'msg' => 'something wrong'];
    }

    public function getFandomSubscriberById(int $userId, int $fandomId): array|bool
    {
        return $this->subscribers->select(['id'])->where(['user_id' => $userId, 'fandom_id' => $fandomId])->get(false);
    }
    public function isUserSubscribed(int $userId,int $fandomId):bool{
        $res = $this->subscribers->select(['user_id'])->where(['user_id'=>$userId,'fandom_id'=>$fandomId])->get(false);
        if(is_array($res)){
            return true;
        }
        return  false;
    }

}