<?php

namespace repositories;

use models\post_likes;

class PostLikeRepository
{
    protected post_likes $post_like;

    public function __construct()
    {
        $this->post_like=new post_likes();
    }
    public function addLike($userId,$postId):bool{
        return $this->post_like->insert(['user_id'=>$userId,'post_id'=>$postId]);
    }
    public function removeLike($postId):bool{
        return $this->post_like->where(['post_id'=>$postId])->delete();
    }

}