<?php

namespace repositories;

use models\user_fandom;

class UserFandomRepository
{
    private user_fandom $user_fandom;

    public function __construct()
    {
        $this->user_fandom = new user_fandom();
    }

    public function addUserFandom(int $userId, int $fandomId): bool
    {
        return $this->user_fandom->insert(['user_id' => $userId, 'fandom_id' => $fandomId]);
    }

    public function getUserFandoms(int $userid): bool|array
    {
        return $this->user_fandom->select('*')->where(['user_id' => $userid])->get();
    }

    public function isUserFandom($fandomId, $userId): array|bool
    {
        $fandom = $this->user_fandom->select(['user_id'])->where(['fandom_id' => $fandomId])->get(false);
        if ($fandom['user_id'] === $userId) {
            return true;
        }
        return false;
    }

}