<?php

namespace repositories;

use models\post;
use models\post_answer;

class PostRepository
{
    protected post $post;
    public function __construct(){
        $this->post = new post();
    }

    public function addPost(int $fandomId,int $senderId, string $content){
        $this->post->insert(['fandom_id'=>$fandomId,'sender_id'=>$senderId,'content'=>$content]);
    }
    public function getFandomPosts($id):array{
        return $this->post->select('post.*,user.login')
            ->join('user',['user.id'=>'post.sender_id'])
            ->where(['fandom_id'=>$id])
            ->get();
    }
    public function getAnswersByPostId($postId):array{
       return $this->post->select('post_answer.*,user.login')
            ->join('post_answer',['post_answer.post_id'=>$postId])
            ->join('user',['user.id'=>'post_answer.sender_id'])
            ->where(['id'=>$postId])->get();
    }
}