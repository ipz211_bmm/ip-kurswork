<?php

namespace repositories;

use core\FileWorker;
use http\Env\Response;
use models\photogalery;

class PhotoRepository
{
    protected photogalery $photogalery;
    protected FileWorker $worker;
   public function __construct(){
       $this->photogalery = new photogalery();
       $this->worker = new FileWorker('files/fandom_img','.jpg');
   }
   public function addPhoto(int $fandomId,string $img):array{
       $name = $this->worker->loadFile($img);
       $res =$this->photogalery->insert(['fandom_id'=>$fandomId,'img'=>$name]);
       return ['result'=>$res,'filename'=>$name];
   }
   public function addPhotos(int $fandomId,array $images):array{
       $response = [];
       foreach ($images as $key=>$image){
           $res = $this->addPhoto($fandomId,$image['tmp_name']);
           $response[]=$res['filename'];
       }
       return $response;
   }
   public function getPhotosByFandomId(int $id):array{
       return $this->photogalery->select('*')->where(['fandom_id'=>$id])->get();
   }
}