<?php

namespace repositories;

use core\FileWorker;
use models\video;

class VideoRepository
{
    protected FileWorker $fileWorker;
    protected video $video;
    public function __construct(){
        $this->fileWorker = new FileWorker('files/fandom_video','.webm');
        $this->video = new video();
    }
    private function addVideo(int $fandomId,$video):string{
        $videoName = $this->fileWorker->loadFile($video);
        $this->video->insert(['fandom_id'=>$fandomId,'src'=>$videoName]);
        return $videoName;
    }

    public function addVideos(int $fandomId, array $videos):array{
        $response = [];
        foreach ($videos as $key=>$video){
            $res = $this->addVideo($fandomId,$video['tmp_name']);
            $response[]=$res;
        }
        return $response;
    }
}