<?php

namespace repositories;

use models\user_post_likes;

class UserPostLikesRepository
{
   protected user_post_likes $post_likes;
    public function __construct()
    {
        $this->post_likes=new user_post_likes();
    }

    public function getPostLikes($userId,$postId):bool|array
    {
        return $this->post_likes->select()->where(['user_id'=>$userId,'post_id'=>$postId])->get(false);
    }
}