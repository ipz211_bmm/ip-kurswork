<?php

namespace repositories;

use models\topic_item;

class TopicItemsRepository
{
   protected topic_item $topic_item;
   public function __construct(){
       $this->topic_item = new topic_item();
   }

   public function addTopicItem(int|string $topicId,int $subtopicId):bool{
      return $this->topic_item->insert(['topic_id'=>$topicId,'subtopic_id'=>$subtopicId]);
   }
}