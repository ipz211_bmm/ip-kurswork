<?php

namespace repositories;

use models\category;

class CategoryRepository
{
    protected category $category;
    public function __construct(){
        $this->category = new category();
    }
    public function getCategories():bool|array{
        return $this->category->select()->get();
    }
}