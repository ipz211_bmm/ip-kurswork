<?php

namespace repositories;

use models\complain_category;

class ComplainCategoryRepository
{
    protected complain_category $complain_category;
  public function __construct(){
      $this->complain_category = new complain_category();
  }
  public function addCategory(string $name):bool{
      return $this->complain_category->insert(['name'=>$name]);
  }

  public function getCategories():array|bool{
      return $this->complain_category->select()->get();
  }
  public function getCategoryByName($name):array{
      return $this->complain_category->select('*')->where(['name'=>$name])->get(false);
  }

}