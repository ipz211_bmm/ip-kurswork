<?php

namespace repositories;

use models\user_comment_likes;

class UserCommentsLikesRepo
{
    protected user_comment_likes $comments_likes;
    public function __construct()
    {
        $this->comments_likes=new user_comment_likes();
    }

    public function getPostLikes($userId,$answerId):array|bool
    {
        return $this->comments_likes->select()->where(['user_id'=>$userId,'answer_id'=>$answerId])->get(false);
    }
}