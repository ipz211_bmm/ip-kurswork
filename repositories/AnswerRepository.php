<?php

namespace repositories;

use models\answer_likes;
use models\post_answer;

class AnswerRepository
{
    protected post_answer $answer;
    protected answer_likes $answer_like;
   public function __construct(){
       $this->answer = new post_answer();
       $this->answer_like = new answer_likes();
   }
    public function addAnswer(string $postId,int $senderId, string $content,string $receiverId=null):bool{
       return $this->answer->insert(['post_id'=>$postId,'content'=>$content,'sender_id'=>$senderId,'receiver_id'=>$receiverId]);
    }
    public function addLike($userId,$postId):bool{
        return $this->answer_like->insert(['user_id'=>$userId,'answer_id'=>$postId]);
    }
    public function removeLike($postId):bool{

        return $this->answer_like->where(['answer_id'=>$postId])->delete();
    }

}