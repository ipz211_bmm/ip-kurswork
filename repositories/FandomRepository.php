<?php

namespace repositories;

use core\FileWorker;
use forms\CreateFandomForm;
use models\fandom;

class FandomRepository
{
    private fandom $fandom;
    public CreateFandomForm $createForm;
    protected FileWorker $fileWorker;
    public function __construct()
    {
        $this->fandom = new fandom();
        $this->createForm = new CreateFandomForm();
        $this->fileWorker = new FileWorker('files/fandom','.jpg');
    }
    public function addFandom($data,$file=null):bool{
        $img = 'em-img.png';
        if($file){
            $img = $this->fileWorker->loadFile($file);
        }
        $this->createForm->loadData($data);
        if(!$this->getFandomByName($data['title'])) {
            if ($this->createForm->validate()) {
                return $this->fandom->insert(['title'=>$data['title'],'content'=>$data['content'],'img'=>$img]);
            }
        }
        return false;
    }

    public function getFandoms(int|string $limit=6):bool|array{
        return $this->fandom->select(['user.firstname,fandom.subscribes,fandom.title,fandom.img,fandom.id'])
            ->join('user_fandom',['fandom.id'=>'user_fandom.fandom_id'])
            ->join('user',['user.id'=>'user_fandom.user_id'])
            ->where(['hidden'=>0])
            ->limit($limit)
            ->get();
    }
    public function updateFandomBody($data):bool{
        return $this->fandom->where(['id'=>$data['id']])->update(['content'=>$data['content']]);
    }
    public function toggleHidden($id,int $hide):bool{
        return $this->fandom->where(['id'=>$id])->update(['hidden'=>$hide]);
    }
    public function getFandomByName($name):bool|array{
        return $this->fandom->select('id')->where(['title'=>$name])->get(false);
    }
    public function getFandomById($id):array|bool{
        return $this->fandom->select()->where(['id'=>$id])->get(false);
    }
    public function updatePhoto($id,$img):bool{
        if($img){
            $img = $this->fileWorker->loadFile($img);
            return $this->fandom->where(['id'=>$id])->update(['img'=>$img]);
        }
        return false;
    }
    public function removeFandom($id):bool{
        $fandom = $this->getFandomById($id);
        if($fandom['img']!=='em-img.png') {
            unlink("files/fandom/" . $fandom['img']);
        }
       return $this->fandom->where(['id'=>$id])->delete();
    }
    public function getSubtopicsByFandomId(int|string $fandomId):array{
        return $this->fandom->select('subtopic.*')
            ->join('topic',['fandom.id'=>'topic.fandom_id'])
            ->join('topic_item',['topic_item.topic_id'=>'topic.id'])
            ->join('subtopic',['subtopic.id'=>'topic_item.subtopic_id'])
            ->where(['id'=>$fandomId])->get();
    }

}