<?php

namespace core;

use models\DbModel;

class Mapper
{
      private DbModel $model;
      public function __construct(DbModel $model){
          $this->model=$model;
      }
      public function map():array{
          return $this->model->select($this->model->mapFields)
              ->where(['id'=>$this->model->id])
              ->get(false);
      }
}