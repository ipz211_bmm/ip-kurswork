<?php

namespace core;

class Template
{
    protected string $path;
    protected array $args = [];

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function setArg($name, $value): void
    {
        $this->args[$name] = $value;
    }

    public function setArgs($args): void
    {
        foreach ($args as $name => $value)
            $this->setArg($name, $value);
    }

    public function getHtml(): string
    {
        ob_start();
        extract($this->args);
        include($this->path);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

}