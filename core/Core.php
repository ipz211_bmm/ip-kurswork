<?php

namespace core;

use controllers\MainController;

class Core
{
    private static Core|null $instance = null;
    public array $app;
    public string $requestMethod;
    private function __construct()
    {

    }

    public static function getInstance(): Core
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function Initialize(): void
    {
        session_start();
        $this->requestMethod= $_SERVER['REQUEST_METHOD'];
    }

    public function Run(): int
    {
        $moduleName = 'main';
        $actionName = 'main';
        $params = null;
        if (!empty($_GET['route'])) {
            $route = $_GET['route'];
            $routeParts = explode("/", $route);
            $moduleName = strtolower(array_shift($routeParts));
            if (!empty($routeParts)) {
                $actionName = strtolower(array_shift($routeParts));
            }
            if(str_contains($route,'$')) {
                $paramParts = explode('$',$routeParts[0]);
                for ($i=0;$i<=count($paramParts)/2;$i++) {
                    $key = $paramParts[$i];
                    $value = $paramParts[$i+1];
                    $params[$key] = $value;
                }
            }else if(!empty($routeParts)){
                $params = $routeParts[0];
            }

        }
        $this->app['moduleName']=$moduleName;
        $this->app['actionName']=$actionName;
        $controllerName = "\\controllers\\" . ucfirst($moduleName) . 'Controller';
        $ctrlActionName = $actionName . 'Action';
        if (!class_exists($controllerName) || !method_exists($controllerName, $ctrlActionName)) {
            $main = new MainController();
            $main->errorAction();
            http_response_code(404);
            die;
        }
        $controller = new $controllerName();
        if($params) {
            $this->app['actionResult'] = $controller->$ctrlActionName($params);
        }else{
            $this->app['actionResult'] = $controller->$ctrlActionName();
        }
        $this->app['title']=ucfirst($actionName);
        return http_response_code(200);
    }

    public function Done():string
    {
         $pathToLayout = 'layouts/layout.php';
         $tpl = new Template($pathToLayout);
         $tpl->setArg('content',$this->app['actionResult']);
         $tpl->setArg('title',$this->app['title']);
         if(isset($_SESSION['user']))
             $tpl->setArg('user',$_SESSION['user']);
         if(is_array($this->app['actionResult'])) {
            return json_encode($this->app['actionResult']);
         }else{
             return $tpl->getHtml();
         }
    }
}