<?php

namespace core;

use JetBrains\PhpStorm\NoReturn;

class Controller
{
    protected string $viewPath;
    private string $moduleName;
    public function __construct(){
        $this->moduleName = Core::getInstance()->app['moduleName'];
        $actionName=Core::getInstance()->app['actionName'];
        $this->viewPath="views/{$this->moduleName}/{$actionName}.php";
    }
    #[NoReturn]
    public function redirect(string $url):void {
        header("Location: {$url}");
        die;
    }

    public function render($viewPath=null,$args=null):string{
        if(empty($viewPath)){
            $tpl=new Template($this->viewPath);
        }else if(!str_contains($viewPath,'/')){
            $tpl=new Template("views/$this->moduleName/$viewPath".'.php');
        }else {
            $tpl = new Template($viewPath.'.php');
        }
        if($args!=null){
            $tpl->setArgs($args);
        }
        return $tpl->getHtml();
    }

}