<?php

namespace core;
include ('config/database.php');

class Db
{
    private static \PDO|null $pdo=null;

    private function __construct(){

    }
    public static function getDb():\PDO{
        if(!empty(self::$pdo)){
            return self::$pdo;
        }
        $db = DATABASE_NAME;
        $host  = DATABASE_HOST;
        $login =DATABASE_LOGIN;
        $password = DATABASE_PASSWORD;
        self::$pdo = new \PDO("mysql: host=$host;dbname=$db",$login,
            $password);
        return self::$pdo;
    }
}
