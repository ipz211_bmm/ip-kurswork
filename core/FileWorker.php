<?php

namespace core;

use models\DbModel;

class FileWorker
{
    private string $photoPath;
    protected string $format;
    public function __construct($photoPath,$format){
        $this->photoPath=$photoPath;
        $this->format=$format;
    }
    public function loadFile($path):string{
        $fileName = $this->generateUniqName();
        move_uploaded_file($path,"$this->photoPath/$fileName");
        return $fileName;
    }
   /* public function savePhoto($base64Data):string{
        $fileName = $this->generateUniqName();
        file_put_contents("$this->photoPath/$fileName",$base64Data);
        return $fileName;
    }*/
    private function generateUniqName():string{
        do {
            $fileName = uniqid() . $this->format;
            $newPath = "$this->photoPath/$fileName";
        } while (file_exists($newPath));
        return $fileName;
    }



}