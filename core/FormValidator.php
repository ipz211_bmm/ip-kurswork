<?php

namespace core;

use core\form\Form;

abstract class FormValidator extends Form
{
    public array $errors = [];
    public const REQUIRED = 'required';
    public const MIN_LEN = 'min';
    public const MAX_LEN = 'max';
    public const EMAIL = 'email';
    public const HIDDEN='hidden';

    public function loadData($data): void
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public abstract function setRules(): array;

    public function validate(): bool
    {
        foreach ($this->setRules() as $field => $rules) {
            $value = $this->{$field};
            foreach ($rules as $key => $rule) {
                $ruleName = $rule;
                if(is_string($key)){
                    $ruleName=$key;
                }
                if ($ruleName === self::REQUIRED && !$value) {
                    $this->addError($field, $ruleName);
                }
                if ($ruleName === self::MIN_LEN && strlen($value) < $rule) {
                    $this->addError($field, $ruleName, $rule);
                }
                if ($ruleName === self::MAX_LEN && strlen($value) > $rule) {
                    $this->addError($field, $ruleName, $rule);
                }
                if ($ruleName === self::EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $this->addError($field, $ruleName);
                }
                if ($ruleName === self::HIDDEN) {
                    $this->{$field}='';
                }
            }
        }
        return empty($this->errors);
    }

    public function addError(string $field, string $rule, $arg=''): void
    {
        $message = $this->errorMessages()[$rule] ?? '';
        $message = str_replace("{{$rule}}", $arg, $message);
        $this->errors[$field][] = $message;
    }

    public function errorMessages():array
    {
        return [
            self::REQUIRED => 'This field is required',
            self::EMAIL => 'Invalid email address',
            self::MAX_LEN => 'Max length of this field must be {max}',
            self::MIN_LEN => 'Min length of this field must be {min}'
        ];
    }


}