<?php

namespace core\form;

class Label
{
    private string $title;
    private string $for;
    private string $class;
    public function __construct(string $title,string $for,string $class=''){
        $this->title=$title;
        $this->for = $for;
        $this->class=$class;
    }
    public function __toString(): string
    {
       return "
       <label for='$this->for' class='$this->class'>
            $this->title
        </label>
       ";
    }
}