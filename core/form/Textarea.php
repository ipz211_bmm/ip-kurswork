<?php

namespace core\form;

class Textarea extends Field
{
    public function __construct($form, string $field, string $label)
    {
        parent::__construct($form, $field, $label);
    }

    public function __toString(): string
    {
        return sprintf("<div>
        <label for='%s'>%s</label>
        <textarea name='%s' id='%s'
         autocomplete='off' value='%s' class='%s'> 
        </textarea>
         <span class='fail'>
         %s
         </span>
        </div>",
            $this->field,
            $this->label,
            $this->field,
            $this->field,
            $this->form->{$this->field}??'',
            empty($this->form->errors[$this->field][0]) ? '' :' input-fail',
            $this->form->errors[$this->field][0]??''
        ) ;
    }
}