<?php

namespace core\form;

class Form
{
   public function begin(string $action,string $method,string $enctype=null,string $class=null):Form{

       echo "<form action='$action' method='$method' enctype='$enctype' $class>";

       return new Form();
   }

   public static function end():void{
       echo '</form>';
   }
   public function select($field,$label):Select{
        return new Select($this,$label,$field);
   }
   public function field(string $field,Label $label,string $type='text',string $class=''):Field{

       return new Field($this,$field,$label,$type,$class);
   }
   public function textarea(string $field,string $label):Textarea{
       return new Textarea($this,$field,$label);
   }
}