<?php

namespace core\form;

use core\FormValidator;

class Field
{
    public Form $form;
    public string $field;
    public string $label;
    public string $type;
    public string $class;
    public function __construct($form, string $field,string $label,string $type='text',string $class='')
    {
        $this->form = $form;
        $this->field = $field;
        $this->label=$label;
        $this->type=$type;
        $this->class=$class;
    }

    public function __toString(): string
    {
        return sprintf("<div>
        %s
        <input type='%s' name='%s' id='%s'
         autocomplete='off' value='%s' class='%s'>
         <span class='fail'>
         %s
         </span>
        </div>",
            $this->label,
            $this->type,
            $this->field,
            $this->field,
            $this->form->{$this->field}??'',
            empty($this->form->errors[$this->field][0]) ? $this->class :' input-fail',
            $this->form->errors[$this->field][0]??''
        ) ;
    }
}