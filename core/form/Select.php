<?php

namespace core\form;

class Select extends Field
{
    public function __construct($form,Label $label,string $field)
    {
        parent::__construct($form,$field,$label);
    }
    public function begin():Select{
        echo "<div>$this->label";
        echo "<select id='$this->field' name='$this->field'>";
        return $this;
    }
    public function option($value){
        echo "<option value='$value'>$value</option>";
    }

    public function end(){
        echo "</select></div>";
    }
}