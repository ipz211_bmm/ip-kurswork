<?php
/* @var array $subtopic **/

?>

<link rel="stylesheet" href="/src/styles/editor.css">
<link rel="stylesheet" href="/src/styles/my-editor.css">
<script src="/src/scripts/subscribe.js" defer></script>

<button class="save-btn">Save</button>
<nav class="navigation">
    <div class="content-block">
        <h3><?=$subtopic['title']?></h3>
    </div>
</nav>
<div class="main">
    <div class="main-content">
        <?= $subtopic['content'] ?>
    </div>
</div>
