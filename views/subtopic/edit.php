<?php
/* @var $subtopic **/
?>
<link rel="stylesheet" href="/src/styles/editor.css">
<link rel="stylesheet" href="/src/styles/my-editor.css">
<link rel="stylesheet" href="/src/styles/subtopic.css">
<script type="module" src="/src/scripts/subtopic.js" defer></script>
<button class="save-btn">Save</button>
<nav class="navigation">
    <div class="content-block">
        <?php if($subtopic): ?>
        <h3><?= $subtopic['title'] ?></h3>
        <?php else: ?>
            <input size="" type="text" value="Untitled topic" id="title">
        <?php endif; ?>
    </div>
    <?php if($subtopic): ?>
    <div class="content-block">
        <button id="del">Remove subtopic</button>
    </div>
    <?php endif; ?>
</nav>
<div class="main">
    <div class="main-content">
        <?php if($subtopic): ?>
            <?=$subtopic['content']?>
        <?php else: ?>
            <h3>Page content</h3>
        <?php endif; ?>
    </div>
    <div id="container">
        <div class="panel">
            <img src="/static/site/font-size.png" alt="increase">
            <img src="/static/site/text.png" alt="decrease">
            <img src="/static/site/bold.png" alt="bold">
            <img src="/static/site/italic.png" alt="italic">
            <img src="/static/site/left.png" alt="left">
            <img src="/static/site/format.png" alt="center">
            <img src="/static/site/right.png" alt="right">
            <label for="img">
                <img src="/static/site/slide-show.png" alt="photo">
            </label>
            <img src="/static/site/ident-right.png" alt="indent-right">
            <img src="/static/site/indent.png" alt="indent-left">
        </div>
        <div id="editor" contenteditable>
            <?php if($subtopic): ?>
                <?=$subtopic['content']?>
            <?php else: ?>
                <h3>Page content</h3>
            <?php endif; ?>
        </div>
    </div>
    <div class="edit">
        <img src="/static/site/pencil.png" alt="edit">
    </div>
</div>
<div class="img-position">
    <img src="/static/site/left.png" alt="left">
    <img src="/static/site/format.png" alt="center">
    <img src="/static/site/right.png" alt="right">
    <img src="/static/site/left-alignment.png" alt="right-text">
    <img src="/static/site/right-alignment.png" alt="left-text">
    <label for="gap">Gap:</label>
    <input type="number" id="gap" name="gap">
</div>
<input type="file" id="img" name="img" value="load" hidden>
<script type="module" src="/src/scripts/edit.js"></script>
<script type="module" src="/src/scripts/editor.js"></script>