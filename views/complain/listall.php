<?php
/* @var array $complains  **/
?>
<link rel="stylesheet" href="/src/styles/complain.css">
<script src="/src/scripts/ban.js" defer></script>
<?php if(empty($complains)): ?>
    <h1 class="msg-center">Complains empty</h1>
<?php endif; ?>

<?php if(!empty($complains)): ?>
<table>
    <th>Fandom name</th>
    <th>Report category</th>
    <th>Description</th>
    <th>Fandom page</th>
    <th>Action</th>
<?php foreach ($complains as $key=>$complain): ?>
<tr>
 <td><?= $complain['title'] ?></td>
 <td><?= $complain['name'] ?></td>
 <td><?= $complain['description'] ?></td>
 <td><a href="/fandom/page/<?=$complain['id']?>">To fandom</a></td>
    <?php if($complain['hidden']==0): ?>
    <td><input type="submit" value="Ban" data-id="<?=$complain['id']?>"></td>
    <?php else: ?>
        <td><input type="submit" value="Remove ban" data-id="<?=$complain['id']?>"></td>
    <?php endif; ?>
</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>