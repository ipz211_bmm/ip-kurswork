<?php
/* @var ReportForm $form**/
/* @var array $categories**/
/* @var array $user_complains**/

use core\form\Label;
use forms\ReportForm;

?>
<link rel="stylesheet" href="/src/styles/forms.css">
<?php if($user_complains): ?>
<h1 class="msg-center">You already complain</h1>
<?php endif; ?>

<?php if(!$user_complains): ?>
<?php $form->begin('','post',null,"class='form'") ?>
<?php $select =  $form->select('category',new Label('Category','category',''))->begin() ?>
<?php foreach ($categories as $key=>$value): ?>
    <?php $select->option($value['name']);?>
<?php endforeach; ?>
<?php $select->end(); ?>
<?php echo $form->textarea('description',new Label('Description','description')) ?>
<input type="submit" value="send">
<?php $form->end(); ?>
<?php endif; ?>

