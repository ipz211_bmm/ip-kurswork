<?php
/* @var array $photos * */
?>
<link rel="stylesheet" href="/src/styles/gallery.css">
<script src="/src/scripts/gallery.js" defer></script>
<div class="gallery-wrapper">
    <?php foreach ($photos as $key => $value): ?>
        <div class="gallery-img-container">
            <img src="/files/fandom_img/<?= $value['img'] ?>" alt="<?= $value['img'] ?>">
            <div class="menu">
                <img src="/static/site/download.png" alt="download">
                <img src="/static/site/save-instagram.png" alt="save">
            </div>
        </div>
    <?php endforeach; ?>
</div>
