<?php
/* @var $fandom * */
/* @var $topics * */
/* @var $ids * */
?>

<script type="module" src="/src/scripts/editor.js" defer></script>
<script type="module" src="/src/scripts/addTopic.js" defer></script>
<script type="module" src="/src/scripts/fandomSave.js" defer></script>
<link rel="stylesheet" href="/src/styles/editor.css">
<link rel="stylesheet" href="/src/styles/my-editor.css">

<button class="save-btn">Save</button>
<nav class="navigation">
    <div class="content-block">
        <h3><?= $fandom['title'] ?></h3>
        <div class="drop-menu">
            <a href="/photo/photogalery/<?= $fandom['id'] ?>">Photo-gallery</a>
            <a href="/video/gallery/<?= $fandom['id'] ?>">Videos</a>
        </div>
    </div>
    <?php foreach ($topics as $key => $value): ?>
        <div class="content-block">
            <h3><?= $key ?></h3>
            <div class="drop-menu">
                <?php if (!empty($value)): ?>
                    <?php foreach ($value as $index => $name): ?>
                        <a href="/subtopic/edit/subtopic$<?= $name['id']?>$topic$<?= $name['top_id']?>$fandom$<?=$fandom['id']?>">
                            <?= $name['title'] ?>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
                <a href="/subtopic/edit/topic$<?= $ids[$key] ?>$<?= $fandom['id']?>">+</a>
                <a data-id="<?= $ids[$key] ?>" class="remove" style="background-color: #ff477e;color: white;">Remove this topic</a>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="content-block">
        <h1 id="new-topic">+</h1>
    </div>
</nav>

<div class="main">
    <div class="main-content">
        <?= $fandom['content'] ?>
    </div>
    <div id="container">
        <div class="panel">
            <img src="/static/site/font-size.png" alt="increase">
            <img src="/static/site/text.png" alt="decrease">
            <img src="/static/site/bold.png" alt="bold">
            <img src="/static/site/italic.png" alt="italic">
            <img src="/static/site/left.png" alt="left">
            <img src="/static/site/format.png" alt="center">
            <img src="/static/site/right.png" alt="right">
            <label for="img">
                <img src="/static/site/slide-show.png" alt="photo">
            </label>
            <img src="/static/site/ident-right.png" alt="indent-right">
            <img src="/static/site/indent.png" alt="indent-left">
        </div>
        <div id="editor" contenteditable>
            <?= $fandom['content'] ?>
        </div>
    </div>
    <div class="edit">
        <img src="/static/site/pencil.png" alt="edit">
    </div>
</div>
<div class="img-position">
    <img src="/static/site/left.png" alt="left">
    <img src="/static/site/format.png" alt="center">
    <img src="/static/site/right.png" alt="right">
    <img src="/static/site/left-alignment.png" alt="right-text">
    <img src="/static/site/right-alignment.png" alt="left-text">
    <label for="gap">Gap:</label>
    <input type="number" id="gap" name="gap">
</div>
<input type="file" id="img" name="img" value="load" hidden>



