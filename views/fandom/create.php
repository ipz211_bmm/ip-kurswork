
<?php
/* @var CreateFandomForm $form **/
/* @var array $categories **/
use forms\CreateFandomForm;
use \core\form\Label;
?>
<link rel="stylesheet" href="/src/styles/forms.css">

<?php $form->begin('','post','multipart/form-data',"class='form'") ?>
<?php echo $form->field('title',new Label('Fandom name','title')) ?>
<?php $select = $form->select('category',new Label('Category','category'))->begin()?>
<?php foreach ($categories as $key=>$category): ?>
<?php $select->option($category['name'])?>
<?php endforeach; ?>
<?php $select->end()?>
<?php echo $form->textarea('content','Description') ?>
<?php echo $form->field('img',new Label('Img (display in main subtopic)','img','file-label'),type: 'file',class:'hide') ?>
<input type="submit" value="Create">
<?php $form->end()?>