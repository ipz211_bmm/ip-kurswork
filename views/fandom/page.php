<?php
/* @var array $fandom * */
/* @var bool $isUserFandom * */
/* @var bool $isUserSubscribed * */
/* @var bool $isUserSubscribed * */
/* @var $topics * */
?>

<link rel="stylesheet" href="/src/styles/editor.css">
<link rel="stylesheet" href="/src/styles/my-editor.css">
<script src="/src/scripts/subscribe.js" defer></script>

<button class="save-btn">Save</button>
<nav class="navigation">
    <?php if (isset($_SESSION['user'])): ?>
        <div class="action-wrapper">
        <?php if (!$isUserFandom): ?>
                <div class="action">
                    <button id="subscribe" class="<?= $isUserSubscribed ? '' : 'subscribe' ?> action-bth">
                        <?= $isUserSubscribed ? 'Unsubscribe' : 'Subscribe' ?>
                    </button>
                    <?php if(isset($_SESSION['user']['permissions'])&&in_array(1,$_SESSION['user']['permissions'])): ?>
                    <button class="action-bth">
                        <a href="/complain/fandom_list/<?=$fandom['id']?>">Complains list</a>
                    </button>
                    <?php endif; ?>
                </div>
                <a href="/complain/create/<?=$fandom['id']?>" class="action">
                    <img src="/static/site/report.png" alt="">
                </a>
                <?php endif; ?>
        <a href="/forum/post/<?=$fandom['id']?>" class="action">
            <img src="/static/site/comment.png" alt="">
        </a>
        </div>
    <?php endif; ?>
    <div class="content-block">
        <h3><?= $fandom['title'] ?></h3>
        <div class="drop-menu">
            <a href="/photo/photogalery/<?= $fandom['id'] ?>">Gallery</a>
        </div>
    </div>
    <?php foreach ($topics as $key => $value): ?>
        <div class="content-block">
            <h3><?= $key ?></h3>
            <div class="drop-menu">
                <?php if (!empty($value)): ?>
                    <?php foreach ($value as $index => $name): ?>
                        <a href="<?= $name['ulr'] ?><?= $name['id'] ?>">
                            <?= $name['title'] ?>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</nav>
<div class="main">
    <div class="main-content">
        <?= $fandom['content'] ?>
    </div>
</div>
