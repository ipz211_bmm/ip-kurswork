<?php
/** @var $user */
/** @var ChangeDataForm $form */
use forms\ChangeDataForm;
use \core\form\Label;

?>
<link rel="stylesheet" href="/src/styles/settings.css">
<div class="info-wrapper">
    <div class="photo">
        <?php if (empty($user['img'])): ?>
            <label for="img" class="file-label">
                <img src="/static/site/no-photo.png" alt="">
            </label>
        <?php else: ?>
        <label for="img" class="file-label">
                <img src="/files/user/<?=$user['img']?>" alt="" class="cover">
        </label>
        <?php endif; ?>
        <h3 id="change-info">
            <?=$_SESSION['msg']??'Натисніть на фото, щоб змінити його'?>
        </h3>
    </div>
    <div class="change-wrapper">
        <?php $form->begin('','POST','multipart/form-data',"class='change-container change-input'") ?>
              <?=$form->field('firstname',new Label('Name','firstname'))?>
              <?=$form->field('surname',new Label('Surname','surname'))?>
              <?=$form->field('login',new Label('Login','login'))?>
              <?=$form->field('email',new Label('Email','email'))?>
               <input type="file" name="img" id="img" hidden>
               <button id="change">Save changes</button>
        <?php $form->end() ?>
    </div>
</div>
