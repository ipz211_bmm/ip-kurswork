<?php
/** @var forms\RegisterForm $form */
use core\form\Label;
?>
<link rel="stylesheet" href="/src/styles/forms.css">
<?php $form->begin('','post',class:"class='form'") ?>
<span class="fail-msg"><?=$form->errors['msg']??''?></span>
<?php echo $form->field('firstname',new Label("Name",'firstname'))?>
<?php echo $form->field('surname',new Label("Surname",'surname'))?>
<?php echo $form->field('login',new Label("Login",'login'))?>
<?php echo $form->field('email',new Label("Email",'email'))?>
<?php echo $form->field('password',new Label("Password",'password'),type: 'password')?>
<input type="submit" value="submit">
<?php $form->end() ?>

