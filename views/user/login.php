<?php
/** @var forms\LoginForm $form */
use \core\form\Label;
?>
<link rel="stylesheet" href="/src/styles/forms.css">
<?php $form->begin('','post',class:"class='form'") ?>
<span class="fail-msg"><?=$form->errors['msg']??''?></span>
<?php echo $form->field('login',new Label("Login / email",'login'))?>
<?php echo $form->field('password',new Label("Password",'password'),type: 'password')?>
<input type="submit" value="submit">
<?php $form->end() ?>
