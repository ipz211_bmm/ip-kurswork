<?php
/* @var $fandoms * */
?>
<link rel="stylesheet" href="/src/styles/main.css">
<link rel="stylesheet" href="/src/styles/studio.css">
<script src="/src/scripts/setPhoto.js" type="module" defer></script>
<?php if (!empty($fandoms)): ?>
    <div class="fun-block-wrapper">
        <div class="fun-container">
            <?php foreach ($fandoms as $key=>$fandom): ?>
                <div class="fun-block-item">
                    <h3><?=$fandom['title']?></h3>
                    <img src="<?='/files/fandom/'.$fandom['img']?>" alt="hp">
                    <div class="edit">
                        <a href="/fandom/editor/<?=$fandom['id']?>">Edit</a>
                        <label for="file">Set new photo</label>
                        <a data-id="<?=$fandom['id']?>" class="delete">Delete</a>
                        <a href="#" id="save-ch" data-id="<?=$fandom['id']?>" style="display: none;">Save Changes</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <h2>У вас ще немає властного фанклубу <a href="/fandom/create">Створити</a></h2>
<?php endif; ?>
<input type="file" hidden id="file" name="file">



