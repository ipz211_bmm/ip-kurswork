<?php
/*  @var array $posts * */

?>
<link rel="stylesheet" href="/src/styles/post.css">
<script type="module" src="/src/scripts/post.js" defer></script>

<div class="post-wrapper">
    <div class="comment-container">
        <form action="" method="post" class="comment-form">
            <textarea class="tex-comment" name="content" id="content" placeholder="YOU COMMENT"></textarea>
            <input type="submit" value="add comment">
        </form>
    </div>
    <?php foreach ($posts as $key => $post): ?>
        <div class="comment-container">
            <div class="comment-header">
                <div class="head-block user-info">
                    <p><?= $post['login'] ?></p>
                </div>
                <!-- <div class="head-block actions">
                    <img src="/static/site/report.png" alt="report">
                </div>
                -->
            </div>
            <p class="comment-content">
                <?= $post['content'] ?>
            </p>
            <div class="comment-action">
                <span><?=$post['likes']?></span>
                <img src="/static/site/heart.png" alt="like" data-id="<?=$post['id']?>">
                <img src="/static/site/speech-bubble.png" alt="answer" data-id="<?=$post['id']?>">
            </div>
        </div>
        <?php if ($post['answers']): ?>
            <?php foreach ($post['answers'] as $index => $answer): ?>
                <div class="comment-container" style="margin-top: -20px;margin-left: 100px;background-color: #8e9aaf;width: 55%;">
                    <div class="comment-header">
                        <div class="head-block user-info">
                            <?php if($answer['receiver_id']): ?>
                            <p><?=$answer['login'].' replay '.$post['login'] ?></p>
                            <?php else: ?>
                                <p><?=$answer['login'] ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <p class="comment-content">
                        <?= $answer['content'] ?>
                    </p>
                    <div class="comment-action" data-user="<?=$answer['sender_id']?>">
                        <span><?=$answer['likes']?></span>
                        <img src="/static/site/heart.png" alt="answer-like" data-id="<?=$answer['id']?>">
                        <img src="/static/site/speech-bubble.png" alt="answer-to" data-id="<?=$post['id']?>">
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

