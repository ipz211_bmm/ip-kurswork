<?php
/* @var $fandoms * */

?>
<link rel="stylesheet" href="/src/styles/main.css">
<script type="module" src="/src/scripts/getFandom.js" defer></script>

<div class="fun-block-wrapper">
    <div class="fun-container">
        <?php foreach ($fandoms as $key=>$fandom): ?>
            <a href="/fandom/page/<?=$fandom['id']?>" class="fun-block-item">
                <h3><?=$fandom['title']?></h3>
                <img src="<?='files/fandom/'.$fandom['img']?>" alt="hp">
                <div class="info">
                    <span><?=$fandom['subscribes']?> followers</span>
                    <?php if(!empty($_SESSION['user'])&&$_SESSION['user']['firstname']===$fandom['firstname']): ?>
                    <span>Creator: you</span>
                    <?php else:?>
                        <span><?=$fandom['firstname']?></span>
                    <?php endif; ?>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<div class="more" id="more">
    <button>More fandoms</button>
</div>
