import postData from "./postFetch.js";

let save = document.getElementById('save-ch');

let file = document.getElementById('file');
let cot = document.querySelector('.fun-container');
file.oninput = ()=>{
    save.style.display='block';
}
save.addEventListener('click',()=>{
    if(file.files[0]){
        postData('/fandom/updatephoto',{'id':save.dataset['id'],'img':file.files[0]}).then(data=>{
            console.log(data);
            save.style.display='none';
            window.location.reload();
        });
    }
})

cot.addEventListener('click',(e)=>{
    if(e.target.classList.contains('delete')) {
        let del = cot.querySelector('.delete');
        let ask = confirm('are you sure?');
        if (ask) {
            postData('/fandom/delete', {'id': del.dataset['id']}).then(data => {
                console.log(data);
            });
        }
    }
})