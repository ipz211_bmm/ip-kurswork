export default async function postData(url = '', data = {},get='json') {
    let form = new FormData();
    for (const [key, value] of Object.entries(data)) {
        form.append(key, value);
    }
    const response = await fetch(url, {
        method: 'POST',
        body: form
    });
    if(get==='text'){
        return await response.text();
    }
    return await response.json();
}