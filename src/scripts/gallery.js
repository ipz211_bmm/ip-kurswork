let wrapper = document.querySelector('.gallery-wrapper');
let panel = document.querySelector('.menu');
function download(url) {
    const anchor = document.createElement("a");
    anchor.href = url;
    anchor.download = url.slice(url.lastIndexOf('/')+1);
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
}
wrapper.addEventListener('click',(e)=>{
    const target = e.target;
    if(target.parentElement.classList.contains('menu')) {
        if (target.tagName === 'IMG') {
            download(target.parentElement.parentElement.firstElementChild.src);
        }
    }
});
