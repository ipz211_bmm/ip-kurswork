import postData from "./postFetch.js";


let more = document.getElementById('more');
let limit = 6;

more.addEventListener('click',()=>{
    postData(`main/fetch/${limit},6`,{},'text').then(data=>{
        let div = document.createElement('div');
        div.innerHTML=data;
        let da = div.querySelector('.fun-container').innerHTML;
        document.body.querySelector('.fun-container').innerHTML += da;
        limit+=6;
    });
});