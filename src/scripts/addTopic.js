import sendData from "./sendData.js";

let addTopic = document.getElementById('new-topic');
let nav = document.querySelector('.navigation');
let plus = null;

function createTitleForm(){
    let input = document.createElement('input');
    input.type='text';
    input.name='title';
    input.id='title';
    let button = document.createElement('button');
    button.id = 'discard';
    button.innerHTML='Discard';
    let saveBth = document.createElement('input');
    saveBth.type='submit';
    saveBth.id='save-title';
    saveBth.name='save-title';
    saveBth.value='Save';
    return {'title':input,'save':saveBth,'discard':button};
}
addTopic.addEventListener('click',()=>{
    let contentBLock = addTopic.parentElement;
    contentBLock.classList.add('flex');
    plus = contentBLock.querySelector('h1');
    contentBLock.innerHTML='';
    let form = createTitleForm();
    for (let item of Object.values(form)) {
        contentBLock.append(item);
    }
    form['save'].addEventListener('click',()=>{
        let lasSlash = window.location.href.lastIndexOf('/');
        let id = window.location.href.slice(lasSlash-1);
        let xhr = sendData({'title':form['title'].value,'id':id},null,'/topic/add');
        xhr.addEventListener('load',()=>{
           let res = JSON.parse(xhr.response);
           if(res['action']){
               contentBLock.innerHTML='';
               contentBLock.append(plus);
               window.location.reload();
           }
        });
    });
   form['discard'].addEventListener('click',()=>{
        contentBLock.innerHTML='';
        contentBLock.append(plus);
    });
});


