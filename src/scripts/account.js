let blocks = document.querySelectorAll('.account');
let imgs = document.querySelectorAll('.animate-img');
let active = null;

function displayBlock(block){
    block.style.display = 'flex';
    block.classList.remove('hide-acc')
    block.classList.add('display-acc');
}
function hideBlock(block){
    block.classList.remove('display-acc');
    block.classList.add('hide-acc');
}
function blockAnimate(img, block) {
    img.addEventListener('click', (e) => {
        img.classList.toggle('bb');
        if (active) {
            hideBlock(active);
        }
        if (!block.classList.contains('display-acc')&&active!==block) {
            displayBlock(block);
            active=block;
            return 0;
        }
        active = null;
    });
    block.addEventListener('animationend', () => {
        if (block.classList.contains('hide-acc')) {
            block.style.display = 'none';
        }
    });
}

window.addEventListener('click', (e) => {
    let target = e.target;
    let  block = target.parentElement.parentElement;
    if (!target.classList.contains('animate-img')&&(!block||!block.classList.contains('account'))) {
        blocks.forEach(item => {
            hideBlock(item);
        });
        active=null;
    }
});

blockAnimate(imgs[0], blocks[0]);
blockAnimate(imgs[1], blocks[1]);


