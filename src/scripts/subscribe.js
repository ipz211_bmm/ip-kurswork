let subscribe = document.getElementById('subscribe');
let clickCount = 0;

async function postData(url = '', data = {}) {
    let form = new FormData();
    for (const [key, value] of Object.entries(data)) {
        form.append(key, value);
    }
    const response = await fetch(url, {
        method: 'POST',
        body: form
    });
    return await response.text();
}

subscribe.addEventListener('click', () => {
    if (clickCount > 4) {
        return 0;
    }
    let lastSlash = window.location.href.lastIndexOf('/');
    let id =  window.location.href.slice(lastSlash+1);
    if (!subscribe.classList.contains('subscribe')) {
        postData('/subscribe/remove', {'fandom_id':id})
            .then(data => {
                console.log(data);
                subscribe.innerHTML = 'Subscribe';
            });
    } else {
        postData('/subscribe/add', {'fandom_id': id})
            .then(data => {
                console.log(data);
                subscribe.innerHTML = 'Unsubscribe';
            });
    }
    subscribe.classList.toggle('subscribe');
});