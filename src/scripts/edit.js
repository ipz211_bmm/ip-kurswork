import sendData from "./sendData.js";
import getFiles from "./editor.js";

export default class EditorHandler{
    isDataSaved=false;
    lastSlash = window.location.href.lastIndexOf('/');
    fandomId = window.location.href.slice(this.lastSlash+1);
    params = [];
    postData = {};
    msg = document.querySelector('.msg');
    constructor(urlSave) {
        this.editorContainer = document.querySelector('#container');
        this.editButton = document.querySelector('.edit');
        this.mainContent = document.querySelector('.main-content');
        this.saveButton = document.querySelector('.save-btn');
        this.urlSave = urlSave;
    }
    changeImgSrc(changeArray) {
        let images = document.querySelector('#editor').querySelectorAll('.photo-wrapper>img:not([src*="/files/fandom_img/"])');
        for (let i = 0; i < images.length; i++) {
            images[i].src = `/files/fandom_img/${changeArray['photosInfo'][i]}`;
            images[i].classList.remove('hover');
        }
        return true;
    }
    changeVideoSrc(changeArray) {
        let videos = document.querySelector('#editor').querySelectorAll('.photo-wrapper>video:not([src*="/files/fandom_video/"])');
        for (let i = 0; i < videos.length; i++) {
            videos[i].src = `/files/fandom_video/${changeArray['photosInfo'][i]}`;
        }
        return true;
    }
    setHover(){
        let images =this.editorContainer.querySelectorAll("img[src*='/files/']");
        for(let img of images){
            img.classList.add('hover');
        }
        let videos = this.editorContainer.querySelectorAll("video[src*='/files/']");
        for(let video of videos){
            video.classList.add('hover');
        }
    }
    removeHover(){
        let images = this.editorContainer.querySelectorAll("img[src*='/files/']");
        for(let img of images){
            img.classList.remove('hover');
            img.classList.remove('selected');
        }
        let videos = this.editorContainer.querySelectorAll("video[src*='/files/']");
        for(let video of videos){
            video.classList.remove('hover');
            video.classList.remove('selected');
        }
    }
    saveFiles(){
        let files = false;
        if (getFiles().img.length > 0) {
            let xt = sendData({'id': this.fandomId}, getFiles().img, '/photo/add');
            this.updateResponse(xt, 'img',this.fandomId);
            files=true;
        }
        if (getFiles().video.length > 0) {
            let xt = sendData({'id': this.fandomId }, getFiles().video, '/video/add');
            this.updateResponse(xt, 'video', this.fandomId);
            files = true;
        }
        return files;
    }
    setBorder() {
        let textBoxes = document.querySelectorAll('.div-tex');
        for (let item of textBoxes) {
            item.style.border = '1px solid lightgray';
            item.setAttribute('contenteditable', true);
        }
    }
    removeBorder() {
        let textBoxes = document.querySelectorAll('.div-tex');
        for (let item of textBoxes) {
            item.style.border = 'none';
            item.setAttribute('contenteditable', false);
        }
    }
    removePanel(editor){
        let panel = editor.querySelector('.img-position');
        if(panel){
            panel.remove();
            return  true;
        }
        return false;
    }
    displayEditor() {
        this.mainContent.style.display = 'none';
        this.editorContainer.style.display = 'block';
        this.saveButton.style.display='block';
        this.setBorder();
        this.setHover();
    }
    hideEditor() {
        this.mainContent.style.display = 'block';
        this.editorContainer.style.display = 'none';
        this.saveButton.style.display = 'none';
        this.removeHover();
        this.removeBorder();
    }
    makeSendData(){
        let editorContent = document.querySelector('#editor');
        this.params['content']=editorContent.innerHTML;
    }
    displayMsg(action,text){
        this.msg.innerText=text;
        this.msg.classList.add(action);
    }
    hideMsg(action){
        this.msg.addEventListener('animationend',()=>{
            this.msg.innerText="";
            this.msg.classList.remove(action);
        })
    }
    updateResponse(XHR, fileType, id) {
        XHR.addEventListener('load', () => {
            if (fileType === 'img') {
                //console.log(XHR.response);
                this.changeImgSrc(JSON.parse(XHR.response))
            }
            if (fileType === 'video') {
               // console.log(XHR.response);
                this.changeVideoSrc(JSON.parse(XHR.response))
            }
            this.removeBorder();
            this.removeHover();
            this.isDataSaved = true;
            this.makeSendData();
            let x = sendData(this.params, null, this.urlSave);
            x.addEventListener('load', () => {
                if(JSON.parse(x.response)['action']){
                    this.displayMsg('head-success','Page updated');
                    this.hideMsg('head-success');
                }else {
                    this.displayMsg('head-fail','Fail page update');
                    this.hideMsg('head-fail');
                }
                this.isDataSaved=true;
            });
        });
    }
    saveContent(){
        let editorContent = document.querySelector('#editor');
        if(!this.saveFiles()) {
            this.removePanel(editorContent);
            this.removeBorder();
            this.removeHover();
            this.makeSendData();
            let a  = sendData(this.params, null, this.urlSave);
            a.addEventListener('load',()=>{
                if(JSON.parse(a.response)['action']){
                    this.displayMsg('head-success','Page updated');
                    this.hideMsg('head-success');
                }else {
                    this.displayMsg('head-fail','Fail page update');
                    this.hideMsg('head-fail');
                }
                this.isDataSaved=true;
            });
        }
    }
    editorToggle(){
        this.editButton.addEventListener('click', () => {
            if (this.editorContainer.style.display === 'block') {
                if (this.isDataSaved) {
                    this.hideEditor();
                    this.isDataSaved=false;
                } else {
                    let conf = confirm('Data does not saved, continue?');
                    if (conf) {
                        this.hideEditor();
                        this.isDataSaved=false;
                    }
                }
                return 0;
            }
            this.displayEditor();
        });
    }
}
