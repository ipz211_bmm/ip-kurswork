import EditorHandler from "./edit.js";
import postData from "./postFetch.js";

const title = document.getElementById('title');
const topicId = window.location.href.match('[subtopic]+\\$+\\d+')[0].slice(-2);
let del = document.getElementById('del');
let handler = new EditorHandler(`/subtopic/addorupdate/${topicId}`);
handler.editorToggle();
handler.saveButton.addEventListener('click', () => {
    if (title) {
        if (title.value === 'Untitled topic') {
            handler.params['title'] = 'Untitled topic';
        } else {
            handler.params['title'] = title.value;
        }
    }
    handler.params['ulr'] = '/subtopic/page/'
    handler.saveContent();
});


del.addEventListener('click',()=>{
    let ask = confirm("are you sure");
    if(ask){
        postData(`/subtopic/delete/${topicId}`).then(
            (data)=>{
                let lastSlash = window.location.href.lastIndexOf('/');
                let id = window.location.href.slice(lastSlash+1);
                if(data['action']){
                    document.body.innerHTML='';
                    let h1 = document.createElement('h1');
                    let a = document.createElement('a');
                    a.href = `/fandom/editor/${id}`;
                    a.innerHTML="To fandom";
                    a.style.padding='5px';
                    h1.classList.add('msg-center');
                    h1.innerHTML="Subtopic was be deleted";
                    h1.append(a);
                    document.body.append(h1);
                }
            }
        );
    }
})
