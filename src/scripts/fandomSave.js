import EditorHandler from "./edit.js";
import postData from "./postFetch.js";

let handler = new EditorHandler('/fandom/update');
let lastSlash = window.location.href.lastIndexOf('/');
const fandomId=window.location.href.slice(lastSlash+1);
handler.editorToggle();
handler.saveButton.addEventListener('click',()=>{
    handler.params['id']=fandomId;
    handler.saveContent();
});
let nav = document.querySelector('.navigation');

nav.addEventListener('click',(e)=>{
   if(e.target.classList.contains('remove')){
       postData(`/topic/delete/${e.target.dataset['id']}`).then((data)=>{
           if(data['action']){
               window.location.reload();
           }
       });
   }
});
