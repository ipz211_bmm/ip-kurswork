let editor = document.getElementById('editor');
let panel = document.querySelector('.panel');
newLine();
let currentBlock = null;
let currentTextDiv = null;
let gap = document.getElementById('gap');
let files = {
    video:[],
    img:[]
};

function setStyle(name, element) {
    switch (name) {
        case 'bold': {
            if(element.style.fontWeight==='bold'){
                element.style.fontWeight = 'normal';
            }else{
                element.style.fontWeight = 'bold';
            }
        }
            break;
        case 'italic': {
            if(element.style.fontStyle==='italic'){
                element.style.fontStyle='';
            }else {
                element.style.fontStyle = 'italic';
            }
        }
            break;
        case 'center': {
            element.style.textAlign = 'center';
        }
            break;
        case 'right': {
            element.style.textAlign = 'right';
        }
            break;
        case 'left': {
            element.style.textAlign = 'left';
        }
            break;
        case 'increase': {
            let size = parseInt(getComputedStyle(element).fontSize);
            element.style.fontSize = `${size += 2}px`
        }
            break;
        case 'decrease': {
            let size = parseInt(getComputedStyle(element).fontSize);
            element.style.fontSize = `${size -= 2}px`
        }
            break;
        case 'indent-left':{
            let margin = parseInt(getComputedStyle(element).marginLeft);
            let editorWidth = parseInt(`${editor.getBoundingClientRect().width}`);
            if(margin<editorWidth) {
                element.style.marginLeft = `${margin += 13}px`;
            }
        }
            break;
        case 'indent-right':{
            let margin = parseInt(getComputedStyle(element).marginLeft);
            if(margin>0) {
                element.style.marginLeft = `${margin-=13}px`;
            }
        }
    }
}
function createPanelInput(labelName,labelFor,inputType){
    if(panel.querySelectorAll("input[type='number']").length>1){
        return 0;
    }
    let label = document.createElement('label');
    label.for = labelFor;
    label.innerHTML=labelName;
    label.classList.add('for');
    let input = document.createElement('input');
    input.type = inputType;
    input.id=labelFor;
    input.name=labelFor;
    panel.append(label);
    panel.append(input);
    let editorRect=editor.getBoundingClientRect();
    if(labelFor==='width') {
        input.oninput = () => {
            if(input.value<parseInt(editorRect.width.toString())) {
                currentBlock.style.width = input.value + 'px';
            }
        }
    }else{
        if(input.value<parseInt(editorRect.height.toString())) {
            input.oninput = () => {
                currentBlock.style.height = input.value + 'px';
            }
        }
    }
}
function removePanelInput(){
    let inputs  = panel.querySelectorAll("input[type='number']");
    let labels = panel.querySelectorAll(".for");
    for (let i of inputs){
        i.remove();
    }
    for (let j of labels){
        j.remove();
    }
}
function setImgStyle(name, wrapper) {
    let texDiv = null;
    let p = null;
    if (name!=='gap'&&name!=='center') {
        let child =wrapper.querySelectorAll('.div-tex');
        if(child.length===1){
            child[0].remove();
        }
    }

    if (name.includes('text')) {
        texDiv = document.createElement('div');
        p = document.createElement('p');
        p.innerText = 'Your text';
        texDiv.setAttribute('contenteditable', true);
        texDiv.classList.add('div-tex');
        texDiv.append(p);
        currentTextDiv = texDiv;
    }
    switch (name) {
        case 'left': {
            wrapper.style.justifyContent = 'flex-start';
        }
            break;
        case 'right': {
            wrapper.style.justifyContent = 'flex-end';
        }
            break;
        case 'center': {
            wrapper.style.justifyContent = 'center';
        }
            break;
        case 'left-text': {
            wrapper.firstChild.before(texDiv);
            wrapper.style.justifyContent = 'flex-end';
        }
            break;
        case 'right-text': {
            wrapper.append(texDiv);
            wrapper.style.justifyContent = 'flex-start';
        }
            break;
        case 'gap':wrapper.style.gap=`${gap.value}px`;
            break;
    }
}

function newLine() {
    let p = document.createElement('p');
    let br = document.createElement('br');
    p.append(br);
    editor.append(p);
}

function displayImg(wrapper) {
    let img = document.createElement('img');
    img.src = URL.createObjectURL(input.files[0]);
    img.classList.add('hover');
    wrapper.append(img);
    editor.append(wrapper);
    newLine();
    currentBlock = img;
}

function displayVideo(wrapper) {
    let video = document.createElement('video');
    video.src = URL.createObjectURL(input.files[0]);
    video.setAttribute('controls', true);
    wrapper.append(video);
    editor.append(wrapper);
    newLine();
    currentBlock = video;
}
export default function getFiles(){
    return files;
}

let Editor = {
    selection: null,
    target: null,
    start: null,
    end: null,
    elements: null,
    startIndex: null,
    endIndex: null,
    setElementStyle: function (target) {
        if(this.endIndex===-1&&this.start){
            setStyle(target.alt,this.start);
        }
        if (Editor.selection.focusNode.parentElement.id !== 'container' && Editor.selection.focusNode.parentElement.id !== 'editor') {
            for (let i = Editor.startIndex; i <= Editor.endIndex; i++) {
                setStyle(target.alt, Editor.elements[i]);
            }
        }
    }
};

panel.addEventListener('click', (e) => {
    Editor.selection = window.getSelection();
    let target = e.target;
    if (target.tagName === 'IMG' || target.id === 'size') {
        if (Editor.selection.focusNode) {
            if (!editor.contains(Editor.selection.focusNode.parentElement)) {
                console.log('ext');
                return 0;
            }
            Editor.start = Editor.selection.getRangeAt(0).startContainer.parentElement;
            Editor.end = Editor.selection.getRangeAt(0).endContainer.parentElement;
            Editor.elements = Array.from(editor.querySelectorAll('*'));
            Editor.startIndex = Editor.elements.indexOf(Editor.start);
            Editor.endIndex = Editor.elements.indexOf(Editor.end);
            Editor.setElementStyle(target);
        }
    }
});

let input = document.getElementById('img');
let setBlock = document.querySelector('.img-position');
editor.addEventListener('click', (e) => {
    const target = e.target;
    if (target.tagName === 'IMG' || target.tagName === 'VIDEO') {
        createPanelInput('Width','width','number');
        createPanelInput('Height','height','number');
        if (target.parentElement.classList.contains('img-position')) {
            setImgStyle(target.alt, target.parentElement.parentElement);
            return 0;
        }
        if(currentBlock) {
            currentBlock.classList.remove('selected');
        }
        target.classList.toggle('selected');
        setBlock.style.display = 'flex';
        let wrap = target.parentElement;
        wrap.append(setBlock);
        currentBlock = target;
    }
});
editor.onkeydown = (e)=>{
    if(e.key==="Backspace"&&e.target.tagName!=='INPUT'){
        let active = editor.querySelector('.selected');
        if(active){
            active.parentElement.remove()
            removePanelInput();
        }
    }
}
window.addEventListener('click', (e) => {
    if (currentTextDiv && currentTextDiv.innerText.length === 1) {
        currentTextDiv.remove();
    }
    if (currentBlock && e.target !== currentBlock && e.target.parentElement !== setBlock&&e.target.parentElement !== gap) {
        currentBlock.classList.remove('selected');
        if(e.target.tagName==='INPUT'&&e.target.type!=='number'){
            removePanelInput();
        }
        setBlock.style.display = 'none';
    }
})
input.oninput = () => {
    let wrapper = document.createElement('div');
    wrapper.classList.add('photo-wrapper');
    wrapper.setAttribute('contenteditable', false);
    if(input.files[0]) {
        if (input.files[0].type.includes('image')) {
            displayImg(wrapper);
            files.img.push(input.files[0]);
        }
        if (input.files[0].type.includes('video')) {
            displayVideo(wrapper);
            files.video.push(input.files[0]);
        }
    }
}
gap.oninput = () =>{
    setImgStyle('gap',gap.parentElement.parentElement);
}

