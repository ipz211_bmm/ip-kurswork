import postData from "./postFetch.js";

let wrapper = document.querySelector('.post-wrapper');
let currentForm=null;
function createAnsForm(id,receiver=null){
    let form = document.createElement('form');
    if(receiver) {
        let hidden = document.createElement('input');
        hidden.type = 'hidden';
        hidden.name = 'receiver_id';
        hidden.id = 'receiver_id';
        hidden.value=receiver;
        form.append(hidden);
    }
    form.method='post';
    form.classList.add('answer');
    let textarea = document.createElement('textarea');
    textarea.name='answer';
    textarea.id='answer';
    let input = document.createElement('input');
    input.type='submit';
    input.value='send';
    form.append(textarea);
    form.append(input);
    return form;
}
wrapper.addEventListener('click',(e)=>{
   const target = e.target;
    if(target.tagName==='IMG'){
        if(target.alt==='answer'||target.alt==='answer-to'){
            if(currentForm){
                currentForm.remove();
                currentForm=null;
                return 0;
            }
            let container = target.parentElement.parentElement;
            let f =  createAnsForm(target.dataset['id'],target.parentElement.dataset['user']);
            container.append(f);
            currentForm = f;
            f.onsubmit = (e)=>{
                e.preventDefault();
                postData(`/forum/answer/${target.dataset['id']}`, Object.fromEntries(new FormData(f))).then(data => {
                    console.log(data);
                });
            }
        }
        if(target.alt==='like'||target.alt==='answer-like'){
            let url = '';
            if(target.alt==='like'){
                url='/forum/like';
            }else{
                url='/forum/likeanswer';
            }
            postData(url, {id: target.dataset['id']}).then(data => {
                let span = target.parentElement.querySelector('span');
                if(data['msg']==='like'||data['msg']==='like_a'){
                    span.textContent = parseInt(span.textContent)+1;
                }else{
                    span.textContent = parseInt(span.textContent)-1;
                }
            });
        }
    }

});