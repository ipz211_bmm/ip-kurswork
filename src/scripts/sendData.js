

export default function sendData(data,files,url) {
    const XHR = new XMLHttpRequest();
    const FD = new FormData();

    for (const [name, value] of Object.entries(data)) {
        FD.append(name, value);
    }
    if(files) {
        for (const [name, value] of Object.entries(files)) {
            FD.append(name, value);
        }
    }
    XHR.open('POST', `${url}`);
    XHR.send(FD);
    return XHR;
}