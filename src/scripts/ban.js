
let table = document.querySelector('table');

async function postData(url = '', data = {}) {
    let form = new FormData();
    for (const [key, value] of Object.entries(data)) {
        form.append(key, value);
    }
    const response = await fetch(url, {
        method: 'POST',
        body: form
    });
    return await response.json();
}

table.addEventListener('click',(e)=>{
    if(e.target.tagName==='INPUT'){
        let id = e.target.dataset['id'];
        let hide = null;
        if(e.target.value==='Ban'){
            hide = 1;
            e.target.value='Remove ban';
        }else{
            hide=0;
            e.target.value='Ban';
        }
        postData('/fandom/hidden',{'id':id,'hide':hide}).then(data=>{
            console.log(data);
        });
    }
});

